//ş
'use strict';

var gulp = require('gulp'),
    del = require('del'),
    sass = require('gulp-sass'),
    csslint = require('gulp-csslint'),
    scsslint = require('gulp-scss-lint'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify');

const packRules = {
    css: {
        debug: [],
        release: []
    }
};

const addPackRuleCss = (debugRule, releaseRule) => {
    packRules.css.debug.push(debugRule);
    packRules.css.release.push(releaseRule);
};

// # Misc
const miscFiles = [
    'owl.video.play.png'
];

const miscFilesDestDir = './public/css/';

gulp.task('misc.clean', function () {
    return del(miscFiles.map(file => miscFilesDestDir + file));
});

gulp.task('misc.copy', ['misc.clean'], function () {
    return gulp
        .src(miscFiles.map(file => './assets/owl-carousel/' + file))
        .pipe(gulp.dest(miscFilesDestDir));
});

gulp.task('js.clean', function () {
    return del('./public/js/app.js');
});

gulp.task('js.pack', ['js.clean'], function () {
    const sourceFiles = [
        './assets/js/jquery-1.8.3.min.js',
        './assets/js/jquery-ui.min.js',
        './assets/js/responsee.js',
        './assets/owl-carousel/owl.carousel.min.js',
        './assets/js/template-scripts.js'
    ];

    return gulp.src(sourceFiles)
        .pipe(concat('app.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./public/js'));
});

// # CSS

// ## Genel
(function (clean, packDebug, packRelease) {
    const sourceFile = './assets/sass/app.scss';

    gulp.task(clean, function () {
        return del('./public/css/app.css');
    });

    gulp.task(packDebug, [clean], function () {
        return gulp.src(sourceFile)
            .pipe(sass.sync({ precision: 8, outputStyle: 'expanded' }).on('error', sass.logError))
            .pipe(gulp.dest('./public/css'));
    });

    gulp.task(packRelease, [clean], function () {
        return gulp.src(sourceFile)
            .pipe(sass.sync({ precision: 8, outputStyle: 'compressed' }).on('error', sass.logError))
            .pipe(gulp.dest('./public/css'));
    });

    addPackRuleCss(packDebug, packRelease);
})(
    'css.clean.genel',
    'css.pack.genel.debug',
    'css.pack.genel.release'
);

// # LINTING
gulp.task('csslint', function () {
    return gulp.src('./public/css/app.css')
        .pipe(csslint('.csslintrc'))
        .pipe(csslint.reporter());
});

gulp.task('scsslint', function () {
    return gulp.src(['./assets/sass/**/*.scss'])
        .pipe(scsslint({ config: '.scss-lint.yml' }));
});

gulp.task('lint', ['csslint', 'scsslint']);
gulp.task('build.debug', ['misc.copy', 'css.pack.debug', 'js.pack']);
gulp.task('build.release', ['misc.copy', 'css.pack.release', 'js.pack']);

gulp.task('default', ['build.release']);
