set LC_ALL=C.UTF-8

# Görevleri al
gulp --tasks |

# İlk iki satırı siktir et
grep -Po '\] [^UT].*' |

# Timestamp'in kapanış köşeli parantezinden önce gelenleri siktir et
grep -Po '\] .*' |

# İçinde cleanup, verify veya bump geçenleri siktir et
grep -Pv '(cleanup|verify|bump)\.' |

# "Ana dal" olmayanları siktir et
grep -Pv '\] [│ ][^─]' |

# Başlangıçta dal ile ilgili saçma karakterleri siktir et
perl -pe 's/\] [├─┬└│]+ ([^\r\n]+)(\r?\n)/$1$2/' |

# Satırları alfabetik biçimde sırala
sort |

# dev.css, dev.js, misc.bumpver.css, prd.css ve prd.js
# satırlarından önce fazladan birer boşluk bırakılmasını sağla
perl -pe 's/(dev\.css|dev\.js|misc\.bumpver\.css|prd\.css|prd\.js)(\r?\n)/$2$1$2/' > gulp-tasks.txt
